import React, { useState, useEffect } from "react";
import './css/home.css';
import CustomSlider from "../component/slider/Slider";
import Nawbar from "../component/nawbar/Nawbar";
import ButtonWhite from "../component/button/button-whit/ButtonWhite";
import Card from "../component/card/Card";
import axios from 'axios';
import ButtonRed from "../component/button/button-red/ButtonRed";
import Footer from "../component/footer/Footer";
import FeedbackForm from "../component/feedback __form/FeedbackForm";

function Home() {

	const [cards, setCards] = useState([]);
	const [basketCards, setBasketCards] = useState([]);
	const [lynx, setLynx] = useState('');
	const [searchValue, setSearchValue] = useState('')


	useEffect(() => {
		axios.get('https://64145c0536020cecfda65872.mockapi.io/data')
			.then(response => {
				setCards(response.data.slice(0, 4));
			})
			.catch(error => {
				console.log(error);
			});
		axios.get('https://64145c0536020cecfda65872.mockapi.io/basket')
			.then(response => {
				setBasketCards(response.data);
			})
			.catch(error => {
				console.log(error);
			});
	}, [])



	const ClickLynx = (e) => {
		alert(`${lynx} подисан на рассылку`)
	}

	const onAddToBasket = (Object) => {
		axios.post('https://64145c0536020cecfda65872.mockapi.io/basket', Object);
		setBasketCards((basketCards) => [...basketCards, Object])
	}

	const onRemoveCard = (id) => {
		axios.delete(`https://64145c0536020cecfda65872.mockapi.io/basket/${id}`);
		setBasketCards((basketCards) => basketCards.filter(Object => Object.id !== id))
	}


	const onChangeSearchValue = (e) => {
		setSearchValue(e.target.value)
	}

	return (
		<div className="Home">

			<div className='width'>
				<Nawbar onAddToBasket={onAddToBasket} basketCards={basketCards} searchValue={searchValue} onChangeSearchValue={onChangeSearchValue} onRemoveCard={onRemoveCard}></Nawbar>
				<CustomSlider></CustomSlider>
				<div className="promotion">
					<h2>Заказывайте мороженое
						<p>и получайте подарки!</p></h2>
					<div className="promotion__content">
						<div className="promotion__content__item">
							<div className="promotion__content__item__txt">
								<h3>Малинка даром!</h3>
								<h4>При покупке 2 кг любого фруктового мороженого добавим в ваш заказ банку малинового варенья бесплатно.</h4>
								<ButtonWhite>Хочу подарок</ButtonWhite>
							</div>
							<div className="promotion__content__item__img">
								<img src="img/promotion__raspberries.png"></img>
							</div>
						</div>
						<div className="promotion__content__item">
							<div className="promotion__content__item__txt">
								<h3>Маршмеллоу даром!</h3>
								<h4>При покупке 2 кг пломбира добавим в ваш заказ упаковку нежных зефирок совершенно бесплатно.</h4>
								<ButtonWhite>Хочу подарок</ButtonWhite>
							</div>
							<div className="promotion__content__item__img">
								<img src="img/promotion__marshmallow.png"></img>
							</div>
						</div>
					</div>
				</div>
				<div className="popular">
					<h2>Попробуйте самые популярные
						<p>вкусы нашего мороженого</p></h2>
					<div className="popular__content">
						{cards.filter(Object => Object.name.toLowerCase().includes(searchValue)).map((Object) => (
							<Card
								imageUrl={Object.imageUrl}
								name={Object.name}
								text={Object.text}
								price={Object.price}
								key={Object.id}
								onBasket={(Object) => onAddToBasket(Object)}
							></Card>
						))}
					</div>
				</div>
				<div className="shop__gleysy">
					<div className="shop__gleysy__content">
						<h2>Магазин Глейси – это онлайн и офлайн-магазин по продаже
							<p>мороженого собственного проиводства на развес</p></h2>
						<div className="shop__gleysy__content__item">
							<div className="shop__gleysy__content__item__content">
								<img src="img/gleysy/foto1.svg" alt="" />
								<h3>Всё наше мороженое изготавливается на собственном производстве с использованием современного оборудования и проверенных временем технологий.</h3>
							</div>
							<div className="shop__gleysy__content__item__content">
								<img src="img/gleysy/foto2.svg" alt="" />
								<h3>Закупка ингредиентов производится только у проверенных фермерских хозяйств, с которыми нас связывает долговременное сотрудничество.</h3>
							</div>
						</div>
						<div className="shop__gleysy__content__item">
							<div className="shop__gleysy__content__item__content">
								<img src="img/gleysy/foto3.svg" alt="" />
								<h3>Для приготовления мороженого используются сливки и молоко высочайшего качества. Все ингредиенты и добавки произведены из натурального сырья.</h3>
							</div>
							<div className="shop__gleysy__content__item__content">
								<img src="img/gleysy/foto4.svg" alt="" />
								<h3>Доставка нашего мороженого осуществляется в специальном термопаке, который не даёт мороженому растаять и позволяет сохранить превосходный вкус.</h3>
							</div>
						</div>
					</div>
				</div>
				<div className="lynx">
					<div className="lynx__txt">
						<div className="lynx__txt__item">
							<h3>Новое в нашем блоге</h3>
							<h2>10 способов сервировки фруктовых щербетов к столу</h2>
						</div>
					</div>
					<div className="lynx__item">
						<div className="lynx__item__content">
							<div className="lynx__item__content__position">
								<h3>
									Подпишитесь на нашу сладкую рассылку и будьте всегда
									в курсе всего самого вкусного, что у нас происходит. Обещаем не спамить и не слать всякой ненужной ерунды. Честно =)
								</h3>
								<div className="lynx__item__content__position__input">
									<input
										type="text"
										placeholder="email@example.com"
										value={lynx}
										onChange={(e) => setLynx(e.target.value)}
									></input>
									<ButtonRed onClick={ClickLynx}>Отправить</ButtonRed>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="delivery">
					<div className="delivery__txt">
						<h2>Доставка любимого мороженого на дом</h2>
						<h3>Хочется полакомиться любимым дессертом, но нет времени съездить в магазин? Закажите доставку мороженого на дом, и курьер привезёт вам ваш заказ в течение часа!</h3>
					</div>
					<div className="delivery__item">
						<div className="delivery__item__position">
							<h3>Укажите адрес и дату доставки, и мы свяжемся с вами, чтобы подтвердить заказ.</h3>
							<div className="delivery__item__position__input1">
								<div className="delivery__item__position__input1__date">
									<h3>Дата</h3>
									<input type="text" placeholder="01.04.2020"></input>
								</div>
								<div className="delivery__item__position__input1__telefon">
									<h3>Телефон</h3>
									<input type="text" placeholder="+7 800 999-00-00"></input>
								</div>
							</div>
							<div className="delivery__item__position__input1__adres">
								<h3>Адрес</h3>
								<input type="text" placeholder="ул. Большая Конюшенная, 19/8"></input>
							</div>
							<div className="delivery__item__button">
								<ButtonRed>Отправить</ButtonRed>
							</div>

						</div>
					</div>
				</div>
				<div className="office__adress">
					<div className="office__adress__item">
						<div className="office__adress__item__content">
							<h3>Адрес главного офиса и офлайн-магазина:</h3>
							<h2>наб. реки Карповки, 5 лит П, Санкт-Петербург</h2>
							<h3>Для заказов по телефону:</h3>
							<h2>+7 (812) 812-12-12</h2>
							<h4>(с 10 до 20 ежедневно)</h4>
							<FeedbackForm></FeedbackForm>
						</div>
					</div>
				</div>
				<Footer></Footer>
			</div>

		</div>
	)

}

export default Home;