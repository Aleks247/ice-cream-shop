import React, { useState, useEffect } from "react";
import './css/catalog.css';
import Nawbar from "../component/nawbar/Nawbar";
import Card from "../component/card/Card";
import axios from 'axios';
import Footer from "../component/footer/Footer";
import Select from "../component/select/Select";


function Catalog() {

	const [cards, setCards] = useState([]);
	const [basketCards, setBasketCards] = useState([]);
	const [searchValue, setSearchValue] = useState('');


	useEffect(() => {
		axios.get('https://64145c0536020cecfda65872.mockapi.io/data')
			.then(response => {
				setCards(response.data);
			})
			.catch(error => {
				console.log(error);
			});
		axios.get('https://64145c0536020cecfda65872.mockapi.io/basket')
			.then(response => {
				setBasketCards(response.data);
			})
			.catch(error => {
				console.log(error);
			});
	}, [])

	const onAddToBasket = (Object) => {
		axios.post('https://64145c0536020cecfda65872.mockapi.io/basket', Object);
		setBasketCards((basketCards) => [...basketCards, Object])
	}

	const onRemoveCard = (id) => {
		axios.delete(`https://64145c0536020cecfda65872.mockapi.io/basket/${id}`);
		setBasketCards((basketCards) => basketCards.filter(Object => Object.id !== id))
	}


	const onChangeSearchValue = (e) => {
		setSearchValue(e.target.value)
	}

	const [sortType, setSortType] = useState(['low-to-high', 'high-to-low', 'low-to-low-popular', 'low-to-high-popular']);

	const sortedProducts = [...cards].sort((a, b) => {
		if (sortType === 'low-to-high') {
			return a.price - b.price;
		} if (sortType === 'high-to-low') {
			return b.price - a.price;
		} if (sortType === 'low-to-high-popular') {
			return a.popular - b.popular;
		} if (sortType === 'low-to-low-popular') {
			return b.popular - a.popular;
		}
	});

	const handleSortChange = (e) => {
		setSortType(e.target.value);
	};


	return (
		<div className="catalog">
			<div className='width'>
				<Nawbar onAddToBasket={onAddToBasket} basketCards={basketCards} searchValue={searchValue} onChangeSearchValue={onChangeSearchValue} onRemoveCard={onRemoveCard}></Nawbar>
				<div className="select__content">
					<h3>Сортировка:</h3>
					<Select onChange={handleSortChange}></Select>
				</div>

				<div className="product">
					{sortedProducts.filter(Object => Object.name.toLowerCase().includes(searchValue)).map((Object) => (
						<Card
							imageUrl={Object.imageUrl}
							name={Object.name}
							text={Object.text}
							price={Object.price}
							key={Object.id}
							onBasket={(Object) => onAddToBasket(Object)}
						></Card>
					))}
				</div>
				<Footer></Footer>
			</div>
		</div>
	)

}

export default Catalog;