import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Home from './pages/Home';
import React from 'react';
import Catalog from './pages/Сatalog';


function App() {



	return (
		<Routes>
			<Route path="/" element={<Home></Home>}></Route>
			<Route path="/catalog" element={<Catalog></Catalog>}></Route>
		</Routes>


	);
}

export default App;
