import React from "react";
import './card.css';


function Card({ name, imageUrl, price, text, onBasket }) {


	const onClickCart = () => {
		onBasket({ name, imageUrl, price })
	}

	return (
		<div className="card" >
			<img src={imageUrl}></img>
			<div className="card__txt">
				<h3>{name}</h3>
				<h4>{text}</h4>
				<div className="card__txt__price">
					<h3>{price} ₽/кг</h3>
					<button className="cart__button"><img src="img/card/cart.svg" alt="" onClick={onClickCart} /></button>

				</div>
			</div>
		</div>
	)

}

export default Card;