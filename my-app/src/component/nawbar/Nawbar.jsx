import React, { useState } from "react";
import './nawbar.css';
import AuthButton from "./auth/AuthButton";
import SearchButton from "./searcheButton/Searche";
import BasketButton from "./basket/Basket";
import { NavLink } from "react-router-dom";

function Nawbar({ basketCards, onRemoveCard, onChangeSearchValue, searchValue, onAddToBasket }) {

	const [burgerMenuOpen, setBurgerMenuOpen] = useState(false);
	const [menuMobileOpen, setMenuMobileOpen] = useState(false)

	const OpenBurgerMenu = () => {
		setBurgerMenuOpen(!burgerMenuOpen)
		setMenuMobileOpen(!menuMobileOpen)
	}

	return (
		<header id="header">
			<div className="nawbar">
				<div className="naw">
					<NavLink to={'/'}><img src="img/nawbar/logo.svg" alt="logo" /></NavLink>
					<ul>
						<li><NavLink to={'/catalog'} searchValue={searchValue} onAddToBasket={onAddToBasket}>Каталог</NavLink></li>
						<li><NavLink to={''}>Доставка и оплата</NavLink></li>
						<li><NavLink to={''}>О компании</NavLink></li>
					</ul>
				</div>
				<div className="nawbar__item">
					<a href="tel: +7 800 555-86-28">+7 800 555-86-28</a>
					<SearchButton searchValue={searchValue} onChangeSearchValue={onChangeSearchValue}></SearchButton>
					<AuthButton></AuthButton>
					<BasketButton basketCards={basketCards} onRemoveCard={onRemoveCard}></BasketButton>
				</div>
			</div>
			<div className="nawbar__mobile">
				<div className="nawbar__mobile__content">
					{burgerMenuOpen ? (
						<div className="burger__menu active" onClick={OpenBurgerMenu}>
							<span className="burger__line burger__line1"></span>
							<span className="burger__line burger__line2"></span>
							<span className="burger__line burger__line3"></span>
							<span className="burger__line burger__line4"></span>
						</div>
					) : (
						<div className="burger__menu" onClick={OpenBurgerMenu}>
							<span className="burger__line burger__line1"></span>
							<span className="burger__line burger__line2"></span>
							<span className="burger__line burger__line3"></span>
							<span className="burger__line burger__line4"></span>
						</div>
					)}

					{menuMobileOpen ? (
						<div className="menu__mobile__open active">
							<div className="naw__mobile">
								<NavLink to={'/'}><img src="img/nawbar/logo.svg" alt="logo" /></NavLink>
								<ul>
									<li><NavLink to={'/catalog'} searchValue={searchValue} onAddToBasket={onAddToBasket}>Каталог</NavLink></li>
									<li><NavLink to={''}>Доставка и оплата</NavLink></li>
									<li><NavLink to={''}>О компании</NavLink></li>
								</ul>
							</div>
						</div>
					) : (
						<div className="menu__mobile__open"></div>
					)}
					<div className="nawbar__item">
					<a href="tel: +7 800 555-86-28">+7 800 555-86-28</a>
					<SearchButton searchValue={searchValue} onChangeSearchValue={onChangeSearchValue}></SearchButton>
					<AuthButton></AuthButton>
					<BasketButton basketCards={basketCards} onRemoveCard={onRemoveCard}></BasketButton>
				</div>
				</div>
			</div>
		</header>

	)

}

export default Nawbar;