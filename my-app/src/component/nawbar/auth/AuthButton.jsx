import React, { useState } from "react";
import './authButton.css';
import InputNawbar from "../inputNawbar/InputNawbar";
import ButtonRed from "../../button/button-red/ButtonRed";

function AuthButton() {


	const [authShow, setAuthShow] = useState(false);


	const clickAuth = () => {
		setAuthShow(!authShow)
	}

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const [passwordShow, setPasswordShow] = useState(false)

	const handleSubmit = (e) => {
		e.preventDefault();
		alert(`Пользователь ${email} вошёл`);

	}


	return (
		<div className="nawbar__item__auth__content">
			<button className="button nawbar__item__auth nawbar__item__button" onClick={clickAuth}><img src={authShow ? 'img/nawbar/close.svg' : 'img/nawbar/auth.svg'} alt="auth" /> Вход</button>
			<div className={authShow ? 'nawbar__item__auth__content__form active' : 'nawbar__item__auth__content__form'}>
				<div>
					<h2>Личный кабинет</h2>
					<InputNawbar
						required
						placeholder="email@example.com"
						type="email"
						value={email}
						onChange={(e) => setEmail(e.target.value)}
					/>
					<InputNawbar
						required
						placeholder="Пароль"
						type={passwordShow ? 'text' : 'password'}
						value={password}
						onChange={(e) => setPassword(e.target.value)}
						onMouseOver={() => setPasswordShow(true)}
						onMouseOut={() => setPasswordShow(false)}
					/>
					<div className="nawbar__item__auth__content__form__entrance">
						<ButtonRed type="submit" onClick={handleSubmit}>Войти</ButtonRed>
						<div className="nawbar__item__auth__content__form__entrance__link">
							<a href="#">Забыли пароль?</a>
							<p><a href="#">Регистрация</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	)

}

export default AuthButton;