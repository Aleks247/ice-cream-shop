import React, {useState} from "react";
import './search.css';
import InputNawbar from '../inputNawbar/InputNawbar';

function SearchButton({onChangeSearchValue, searchValue}) {

	const [searchShow, setSearchShow] = useState(false);

	const clickSearch = () => {
		setSearchShow(!searchShow);
	}

	return (
		<div className="nawbar__item__search__content">
			<button className="nawbar__item__search nawbar__item__button" onClick={clickSearch}><img src={searchShow ? 'img/nawbar/close.svg' : 'img/nawbar/search.svg'} alt="search" /></button>
			<div className={searchShow ? 'nawbar__item__search__content__input active' : 'nawbar__item__search__content__input'}>
				<InputNawbar
				value={searchValue}
				onChange={onChangeSearchValue} 
				type="text" 
				placeholder="Поиск по сайту"></InputNawbar>
			</div>
		</div>
	)

}

export default SearchButton;