import React, { useState } from "react";
import ButtonRed from "../../button/button-red/ButtonRed";
import './basket.css';
import axios from 'axios';

function BasketButton({ basketCards = [], onRemoveCard }) {

	const [openBasket, setOpenBasket] = useState(false);

	const ClisckBasketButton = () => {
		setOpenBasket(!openBasket)
	}

	const total = basketCards.reduce((sum, Object) => sum + parseInt(Object.price), 0);

	return (
		<div className="nawbar__item__basket__content">
			{basketCards.length === 1 ? (
				<button onClick={ClisckBasketButton} className="button nawbar__item__basket nawbar__item__button"><img src={openBasket ? "img/nawbar/close.svg" : "img/nawbar/basket-black.svg"} alt="basket" /> {basketCards.length} товар</button>
			) : basketCards.length > 1 && basketCards.length < 5 ? (
				<button onClick={ClisckBasketButton} className="button nawbar__item__basket nawbar__item__button"><img src={openBasket ? "img/nawbar/close.svg" : "img/nawbar/basket-black.svg"} alt="basket" /> {basketCards.length} товарa</button>
			) : basketCards.length === 5 ? (
				<button onClick={ClisckBasketButton} className="button nawbar__item__basket nawbar__item__button"><img src={openBasket ? "img/nawbar/close.svg" : "img/nawbar/basket-black.svg"} alt="basket" /> {basketCards.length} товаров</button>
			) : basketCards.length > 5 ? (
				<button onClick={ClisckBasketButton} className="button nawbar__item__basket nawbar__item__button"><img src={openBasket ? "img/nawbar/close.svg" : "img/nawbar/basket-black.svg"} alt="basket" /> {basketCards.length} товаров</button>
			) : (
				<button onClick={ClisckBasketButton} className="button nawbar__item__basket nawbar__item__button"><img src={openBasket ? "img/nawbar/close.svg" : "img/nawbar/basket.svg"} alt="basket" /> корзина</button>
			)}
			<div className={openBasket ? "nawbar__item__basket__content__item active" : "nawbar__item__basket__content__item"}>
				{basketCards.length > 0 ? (
					<div className="nawbar__item__basket__content__item__position">
						<h2>Корзина</h2>
						<div className="card__basket__box">

							{basketCards.map((Object) =>
								<div className="card__basket" key={Object.id}>
									<div className="card__basket__item">
										<img src={Object.imageUrl} alt="" />
										<div className="card__basket__item__name">
											<h3>{Object.name}</h3>
											<h4>1 кг х {Object.price} ₽</h4>
										</div>
									</div>
									<h3>{Object.price} ₽</h3>
									<img onClick={() => onRemoveCard(Object.id)} src="img/close.svg" alt="" id="card__basket__close" />
								</div>
							)}

						</div>
						<div className="basket__button__вesign">
							<ButtonRed>Оформить заказ</ButtonRed>
							<h3>Итого: {total}</h3>
						</div>

					</div>
				) :
					(
						<div><img src="img/basket/cart__is__empty.png" className="cart__is__empty"></img>
							<h2>Корзина пуста</h2>
						</div>

					)
				}
			</div>
		</div>

	)

}

export default BasketButton;