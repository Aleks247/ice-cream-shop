import React from "react";
import './inputNawbar.css';

function InputNawbar({...props}) {

	return(
		<input className="input__nawbar" {...props}></input>
	)

}

export default InputNawbar;