import React, { useState } from "react";
import ButtonRed from "../button/button-red/ButtonRed";
import './feedbackForm.css';

function FeedbackForm() {
	const [openFeedbackForm, setOpenFeedbackForm] = useState(false)

	const ClickFeedbackForm = () => {
		setOpenFeedbackForm(true)
	}

	const CloseFeedbackForm = () => {
		setOpenFeedbackForm(false)
	}

	const handleOverlayClick = (event) => {
		if (event.target === event.currentTarget) {
			CloseFeedbackForm();
		}
	  };

	return (
		<div>
			<ButtonRed onClick={ClickFeedbackForm}>Форма обратной связи</ButtonRed>
			{ClickFeedbackForm && (
					<div className={openFeedbackForm ? "feedback__form active" : "feedback__form"}  onClick={handleOverlayClick}>
					<div className={openFeedbackForm ? "feedback__form__item active" : "feedback__form__item"}>
						<img onClick={CloseFeedbackForm}  src="img/feedback__form/close.svg" alt="" />
						<div className="feedback__form__item__content">
							<h2>Мы обязательно ответим вам!</h2>
							<input type="text" placeholder="Имя и фамилия"></input>
							<input type="email" placeholder="email@example.com"></input>
							<textarea className="feedback__form__item__content__texterea" placeholder="В свободной форме"></textarea>
						</div>
						<ButtonRed id="feedback__form__button">Отправить</ButtonRed>
					</div>
				</div>
			)}
		</div>

	)

}

export default FeedbackForm;