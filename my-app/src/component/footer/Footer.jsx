import React from "react";
import './footer.css';

function Footer() {

	

	return (
		<footer id="footer">
			<div className="footer__sociaд__network">
				<a href="#"><img src="img/footer/foto1.svg" alt="" /></a>
				<a href="#"><img src="img/footer/foto2.svg" alt="" /></a>
				<a href="#"><img src="img/footer/foto3.svg" alt="" /></a>
			</div>
			<div className="footer__info">
				<img src="img/footer/heart.svg" alt="" />
				<div className="footer__info__for__suppliers footer__info__position">
					<div className="footer__info__for__suppliers__txt">
						<h3>Для поставщиков</h3>
					</div>
					<h3>О производстве</h3>
				</div>
				<div className="footer__info__link footer__info__position">

					<h3><a href="#">Наши документы</a></h3>
					<h3><a href="#">Экологические стандарты</a></h3>
				</div>
			</div>

		</footer>
	)

}

export default Footer;