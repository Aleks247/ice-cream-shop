import React from "react";
import './select.css';


function Select({ onChange }) {


	return (
		<select id="sort-by" onChange={onChange}>
			<option className="sort-by__item">Сортировать по...</option>
			<option value="high-to-low">По уменьшению цены </option>
			<option value="low-to-high">По увеличению цены</option>
			<option value="low-to-low-popular">По уменьшению популярности</option>
			<option value="low-to-high-popular">По увеличению популярности</option>
		</select>
	)

}

export default Select;
