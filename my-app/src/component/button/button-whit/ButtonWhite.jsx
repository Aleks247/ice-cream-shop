import React from "react";
import './buttonWhit.css';

function ButtonWhite({children}) {

	return(
		<button className="button-white">{children}</button>
	)

}

export default ButtonWhite;