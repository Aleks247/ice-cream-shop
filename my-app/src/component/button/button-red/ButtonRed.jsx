import React from "react";
import './buttonRed.css';

function ButtonRed({children, ...props} ) {

	return(
		<button className="button-red" {...props}>{children}</button>
	)

}

export default ButtonRed;