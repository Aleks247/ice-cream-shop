import React, { Component } from "react";
import Slider from "react-slick";
import './slider.css'
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import ButtonWhite from "../button/button-whit/ButtonWhite";

function CustomSlider() {

	const settings = {

		dots: false,
		infinite: true,
		speed: 500,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 3000,
		arrows:false,
		beforeChange: (currentSlide, nextSlide) => {
			// здесь можно изменить цвет фона страницы в зависимости от текущего и следующего слайда
			if (nextSlide === 0) {
				document.body.style.backgroundColor = "#FEAFC3";
				document.body.style.transition = "0.3s";
			} else if (nextSlide === 1) {
				document.body.style.backgroundColor = "#69A9FF";
			} else {
				document.body.style.backgroundColor = "#FCC850";
			}
		}
	};

	return (
		<Slider {...settings}>
			<div>
				<div className="slide__item">
					<div className="slide__item__info">
						<div className="slide__item__info__position">
							<h2>
								Нежный пломбир с клубничным джемом
							</h2>
							<h3>
								Натуральное мороженое из свежих сливок и молока с вкуснейшим клубничным джемом – это идеальный десерт для всей семьи.
							</h3>
							<ButtonWhite>Заказать</ButtonWhite>
						</div>

					</div>
					<div className="slide__item__img">
						<div className="slide__item__img__circle"></div>
						<img src="img/slider/ice__cream1.png"></img>
					</div>
				</div>
			</div>
			<div>
				<div className="slide__item">
					<div className="slide__item__info">
						<div className="slide__item__info__position">
							<h2>
								Сливочное мороженое со вкусом банана
							</h2>
							<h3>
								Сливочное мороженое с ярким банановым вкусом подарит вам свежесть и наслаждение даже в самый жаркий летний день.
							</h3>
							<ButtonWhite>Заказать</ButtonWhite>
						</div>

					</div>
					<div className="slide__item__img">
					<div className="slide__item__img__circle"></div>
						<img src="img/slider/ice__cream2.png"></img>
					</div>
				</div>
			</div>
			<div>
				<div className="slide__item">
					<div className="slide__item__info">
						<div className="slide__item__info__position">
							<h2>
								Карамельный пломбир
								с маршмеллоу
							</h2>
							<h3>
								Необычный сладкий десерт с карамельным топпингом и кусочками зефира завоюет сердца сладкоежек всех возрастов.
							</h3>
							<ButtonWhite>Заказать</ButtonWhite>
						</div>

					</div>
					<div className="slide__item__img">
					<div className="slide__item__img__circle"></div>
						<img src="img/slider/ice__cream3.png"></img>
					</div>
				</div>
			</div>
		</Slider>
	);
}

export default CustomSlider;


